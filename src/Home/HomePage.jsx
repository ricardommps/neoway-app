import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';
import { Form, Field, reduxForm, isValid, isSubmitting, getFormValues } from 'redux-form';

import { cloneDeep } from 'lodash';
import classNames from 'classnames';

import { getClients, getClientById, updateClient, deleteClient, getClientByCpfCnpj } from '../redux/clients/actions';
import { getItensClients, showClient, clientUpdate } from '../redux/clients/selectors';

import { ModalConfirm } from '../common/UI'
import { ToastManager } from '../common/UI';
import { obrigatorio, formatCpfCnpj, validateCpfCnpj } from '../common/validation/validation';

import MaterialTable from 'material-table'

import Warning from '@material-ui/icons/Warning';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import InputBase from '@material-ui/core/InputBase';
import { withStyles } from '@material-ui/core/styles';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import SearchIcon from '@material-ui/icons/Search';
import Button from '@material-ui/core/Button'
import Fab from '@material-ui/core/Fab';

import { styles } from './homeStyle'


const renderInputBase= ({
    input,
    label,
    meta: { touched, error },
    ...custom
}) => (
    <InputBase
        placeholder="Search Cpf/Cnpj"
        {...input}
        {...custom}
    />
    )


class HomePage extends Component {

    constructor(props) {
        super(props);

        this.state = {
            openModalConfim: false,
            actionModal:null,
            modalMsg:"",
            clientSelectd: {}
        };

        this.handleClickEdit = this.handleClickEdit.bind(this);
        this.handleClickNewClient = this.handleClickNewClient.bind(this);
        this.handleSearch = this.handleSearch.bind(this)

    }

    componentDidMount() {
        console.log(">>>HomePage componentDidMount")
        this.props.getClients();
    }

    // Modal 
    handleConfirme = () => {
        console.log(">>>>handleConfirme<<<",this.state.actionModal)
        const clientSelectd = cloneDeep(this.state.clientSelectd);
        const action = this.state.actionModal;
        console.log(">>>>action",action)
        this.setState({
            openModalConfim: false,
            clientSelectd: {},
            actionModal:null,
            modalMsg:""
        });
        if(action == 'blackList'){
            this.props.updateClient(clientSelectd, clientSelectd._id)
            .then(() => {
                ToastManager.showSuccessMessage(`Cpf/Cnpj ${this.props.clientEdit.cpfCnpj} ${this.props.clientEdit.blackList ? " adicionado " : " removido "}a Black-List`);
                this.props.getClients();
            })
            .catch(err => {
                console.log(">>err", err)
            })
        }else if(action == 'deleteClient'){
            this.props.deleteClient(clientSelectd._id)
            .then(() => {
                console.log(">>>BLACKLIST>>", this.props.clientEdit)
                ToastManager.showSuccessMessage(`Cliente removido com sucesso!`);
                this.props.getClients();
            })
            .catch(err => {
                console.log(">>err", err)
            }) 
        }
        
    }

    handleClose = () => {
        this.setState({
            openModalConfim: false,
            clientSelectd: {}
        });
    }

    // Black List
    handleClickBlackList = item => {
        const clientSelectd = cloneDeep(item)
        clientSelectd.blackList = clientSelectd.blackList ? false : true
        const modalMsg = `Deseja <b>${clientSelectd.blackList ? "adicionar ": "remover "}</b>o Cpf/Cnpj <b>${clientSelectd.cpfCnpj}</b> a Black-List ?`
        this.setState({
            openModalConfim: true,
            clientSelectd: clientSelectd,
            actionModal:'blackList',
            modalMsg:modalMsg
        });
    };

    //Edit Client
    handleClickEdit = (idClient) => {
        this.props.history.push(`/updateClient/${idClient}`);
    }

    //Delete Client
    handleDeletClient = (item) => {
        const clientSelectd = cloneDeep(item)
        const modalMsg = `Deseja <b>remover</b> o cliente com o Cpf/Cnpj <b>${clientSelectd.cpfCnpj}</b>?`
        this.setState({
            openModalConfim: true,
            clientSelectd: clientSelectd,
            actionModal:'deleteClient',
            modalMsg:modalMsg
        });
    }

    //New Client
    handleClickNewClient = () => {
        this.props.history.push('/newClient');
    }

    handleSearch = (search) => {
        console.log(">>>handleSave",search.cpfCnpj)
        this.props.getClientByCpfCnpj(search.cpfCnpj)
            .then(() => {
               if(this.props.clientById.length == 0){
                ToastManager.showWarningMessage(`Nenhum cliente encontrado`);
               }else{
                this.props.history.push(`/updateClient/${this.props.clientById._id}`);
               }
            })
            .catch(err => {
                console.log(">>err", err)
            }) 
        
    }


    render() {
        console.log(">>>PROPS", this.props)
        const { clients, classes, handleSubmit, valid, submitting } = this.props
        return (
            <React.Fragment>
                <div className={classes.root}>
                    <AppBar position="static">
                        <Toolbar>
                        <Button
                            variant="contained"
                            className={classNames(classes.margin, classes.cssRoot)}
                            onClick={() => this.handleClickNewClient()}
                        >
                           Novo cliente
                        </Button>
                            <div className={classes.grow} />
                            <Form onSubmit={handleSubmit(this.handleSearch)} className={classes.formSearch}>
                                <div className={classes.search}>
                                    <Field
                                        autoComplete="cpfCnpj"
                                        name="cpfCnpj"
                                        type="text"
                                        component={renderInputBase}
                                        classes={{
                                            root: classes.inputRoot,
                                            input: classes.inputInput,
                                        }}
                                        validate={[
                                            obrigatorio,
                                            validateCpfCnpj
                                        ]}
                                        normalize={formatCpfCnpj}
                                    />
                                    
                                </div>
                                <Fab 
                                    size="small" 
                                    color="secondary" 
                                    aria-label="Add" 
                                    className={classes.margin}
                                    type="submit" 
                                    disabled={!valid || submitting}>
                                    <SearchIcon />
                                </Fab>
                            </Form>
                        </Toolbar>
                    </AppBar>
                </div>
                <div>
                    <MaterialTable

                        columns={[
                            { title: 'First Name', field: 'firstName' },
                            { title: 'Last Name', field: 'lastName' },
                            { title: 'Email', field: 'email' },
                            { title: 'Cpf/Cnpj', field: 'cpfCnpj' },
                            {
                                title: 'Black-List',
                                field: 'blackList',
                                render: rowData => {
                                    console.log(">>>rowData", rowData.blackList)
                                    return (

                                        <IconButton onClick={() => this.handleClickBlackList(rowData)} className={!rowData.blackList ? classes.blacklist : null}>
                                            <Warning fontSize="small" />
                                        </IconButton>

                                    )
                                },
                            },
                            {
                                title: 'Actions',
                                field: '_id',
                                render: rowData => {
                                    return (
                                        <div>
                                            <IconButton onClick={() => this.handleClickEdit(rowData._id)}>
                                                <EditIcon fontSize="small" />
                                            </IconButton>

                                            <IconButton onClick={() => this.handleDeletClient(rowData)}>
                                                <DeleteIcon fontSize="small" />
                                            </IconButton>
                                        </div>

                                    )
                                },
                            }
                        ]}
                        data={clients}
                        title="Clients"
                    />
                </div>
                <ModalConfirm
                    openModal={this.state.openModalConfim}
                    clientSelectd={this.state.clientSelectd}
                    handleConfirme={this.handleConfirme}
                    handleClose={this.handleClose}
                    modalMsg={this.state.modalMsg}
                />
            </React.Fragment>
        )
    }
}

const FORM_ID = 'form_search_client';

const mapStateToProps = state => ({
    clients: getItensClients(state),
    clientById: showClient(state),
    clientEdit: clientUpdate(state),
    formCriarContaValues: getFormValues(FORM_ID)(state),
    valid: isValid(FORM_ID)(state),
    submitting: isSubmitting(FORM_ID)(state),
    initialValues: showClient(state)
});

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            getClients,
            getClientById,
            updateClient,
            deleteClient,
            getClientByCpfCnpj
        },
        dispatch
    );

export default compose(
    withStyles(styles),
    connect(mapStateToProps, mapDispatchToProps),
    reduxForm({ form: FORM_ID, touchOnChange: true, touchOnBlur: false, enableReinitialize: true })
)(HomePage);