import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';
import { Form, Field, reduxForm, isValid, isSubmitting, getFormValues } from 'redux-form';

import { updateClient, createClient, getClientById } from '../redux/clients/actions';
import { getItensClients, showClient } from '../redux/clients/selectors';

import { obrigatorio, validateEmail, formatCpfCnpj, validateCpfCnpj, formatPhone } from '../common/validation/validation';
import { ToastManager } from '../common/UI';

import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

import types from '../constants/actionTypes.constants';

import {styles} from './clientStyle'


const renderTextField = ({
    input,
    label,
    meta: { touched, error },
    ...custom
}) => (
        <TextField
            error={Boolean(touched && error)}
            id={`standard_${label}`}
            label={label}
            helperText={touched && error}
            margin="normal"
            fullWidth
            {...input}
            {...custom}
        />
    )


class ClientPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            typePerson: 'fisica',
        }
        this.handleSave = this.handleSave.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange = event => {
        this.setState({ typePerson: event.target.value });
    };

    componentDidMount() {
        const { idClient } = this.props.match.params;
        if (idClient) {
            this.props.getClientById(idClient)
                .then((result) => {
                    console.log("CLIENTBYID>>", result)
                    this.props.initialize(this.props.clientById)
                })
        }
    }

    componentWillUnmount() {
        this.props.dispatch({ type: types.CLEAN })
    }


    handleSave = () => {
        const client = this.props.formCriarContaValues
        console.log(">>>>handleSave>>>>",client)
        if(!this.props.match.params.idClient){
            this.saveClient(client)
        }else{
            this.editClient(client,this.props.match.params.idClient)
        }

       
    }

    saveClient = (client) => {
        this.props.createClient(client)
            .then((response) => {
                ToastManager.showSuccessMessage("Cliente criado com sucesso!");
                this.props.history.push('/client');
            })
            .catch(err => {
                console.log(">>err", err)
            })
    }

    editClient =(client, idClient) => {
        delete client._id;
        delete client.cpfCnpj;
        console.log(">>>>>editClient",client)
        this.props.updateClient(client, idClient)
            .then(() => {
                ToastManager.showSuccessMessage("Cliente editado com sucesso!");
                this.props.history.push('/client');
            })
            .catch(err => {
                console.log(">>err", err)
            })
    }


    render() {
        const { clientById, handleSubmit, pristine, reset, valid, submitting, classes } = this.props;
        const typePerson = this.state.typePerson
        console.log(">>>PROPS>>>", this.props)
        return (
            <Grid
                container
                spacing={0}
                alignItems="center"
                justify="center"
                style={{ minHeight: '100vh' }}
            >
                <Grid item xs={8}>
                    <Paper className={classes.control}>
                    <Typography variant="h5" component="h3">
                       Cadastro de Cliente
                    </Typography>
                        <Form onSubmit={handleSubmit(this.handleSave)}>
                            <Grid item xs container spacing={16}>
                                <Grid item xs={6}>
                                    <Field
                                        autoComplete="firstName"
                                        name="firstName"
                                        type="text"
                                        component={renderTextField}
                                        label="First Name"
                                        validate={[obrigatorio]}
                                    />
                                </Grid>
                                <Grid item xs={6}>
                                    <Field
                                        autoComplete="lastName"
                                        name="lastName"
                                        component={renderTextField}
                                        type="text"
                                        label="Last Name"
                                        validate={[obrigatorio]}
                                    />
                                </Grid>
                                <Grid item xs={6}>
                                    <Field
                                        autoComplete="email"
                                        name="email"
                                        component={renderTextField}
                                        type="email"
                                        label="Email"
                                        validate={[obrigatorio, validateEmail]}
                                    />
                                </Grid>
                                <Grid item xs={6}>
                                    <Field
                                        autoComplete="phone"
                                        name="phone"
                                        component={renderTextField}
                                        type="text"
                                        label="Phone"
                                        validate={[obrigatorio]}
                                        normalize={formatPhone}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <Field
                                        autoComplete="address"
                                        name="address"
                                        component={renderTextField}
                                        type="text"
                                        label="Address"
                                        validate={[obrigatorio]}
                                    />
                                </Grid>
                                <Grid item xs={6}>
                                    <Field
                                        autoComplete="cpfCnpj"
                                        name="cpfCnpj"
                                        component={renderTextField}
                                        type="text"
                                        label="CPF/CNPJ"
                                        validate={[
                                            obrigatorio,
                                            validateCpfCnpj
                                        ]}
                                        normalize={formatCpfCnpj}
                                        disabled={this.props.match.params.idClient ? true : false}
                                    />
                                </Grid>
                                <Grid item xs={8}>
                                    <Button 
                                        variant="contained" 
                                        color="primary" 
                                        className={classes.button}
                                        type="submit" 
                                        disabled={!valid || submitting}
                                    >
                                        Salvar
                                    </Button>

                                    <Button 
                                        variant="contained" 
                                        color="secondary" 
                                        className={classes.button}
                                        type="submit" 
                                        disabled={pristine || submitting}
                                        onClick={reset}
                                    >
                                        {this.props.match.params.idClient ? "Resetar edição" : "Limpara campos"}
                                    </Button>           
                                </Grid>
                            </Grid>
                        </Form>
                    </Paper>

                </Grid>
            </Grid>
        )
    }
}

const FORM_ID = 'form_create_client';

const mapStateToProps = state => ({
    clientById: showClient(state),
    formCriarContaValues: getFormValues(FORM_ID)(state),
    valid: isValid(FORM_ID)(state),
    submitting: isSubmitting(FORM_ID)(state),
    initialValues: showClient(state),
});

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            getClientById,
            createClient,
            updateClient
        },
        dispatch
    );

export default compose(
    withStyles(styles),
    connect(mapStateToProps, mapDispatchToProps),
    reduxForm({ form: FORM_ID, touchOnChange: true, touchOnBlur: false, enableReinitialize: true })
)(ClientPage);


