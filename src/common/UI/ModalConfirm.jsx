import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const innerHTML = (str) => {
    return {__html: str};
}

export default class ModalConfirm extends Component {

    render() {
        const {openModal, clientSelectd, handleConfirme, handleClose, modalMsg} = this.props
        return (
            <Dialog
                open={openModal}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                >
                <DialogTitle id="alert-dialog-title">{"Atenção"}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        <div dangerouslySetInnerHTML={innerHTML(modalMsg)} />
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleConfirme} color="primary">
                        Confirmar
                    </Button>
                    <Button onClick={handleClose} color="primary" autoFocus>
                        cancelar
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }
}